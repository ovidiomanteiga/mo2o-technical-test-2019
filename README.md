# MO2O iOS Technical Test (2019)

Recipe searching app using the RecipePuppy API: http://www.recipepuppy.com/about/api/

# Requirements

- ✅ User should be able to insert the name of a recipe in the search and to get the searching result in a list.
- ✅ Every time user enters or deletes a character from the search, the list must to be updated.

# Extras

Additional features that give more value to the test:

- ✅ Detail screen.
- ❌ Paginated search.
- ❌ Ingredients filters.
- ✅ Unit tests.

# Highlights

- ✅ Clean Architecture.
- ✅ Dependency injection (Composition).
- ✅ Clean Code style.
- ✅ Mocked remote services.
- ✅ No 3rd-party packages used.

---

Author: Ovidio Manteiga Moar.
