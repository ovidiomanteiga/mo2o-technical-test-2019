//
//  RecipeSearchInteractorTests.swift
//  MO2O Recipe SearchTests
//
//  Created by Ovidio Manteiga Moar on 29/03/2019.
//  Copyright © 2019 ovidiomm. All rights reserved.
//

import XCTest

@testable import MO2O_Recipe_Search


class RecipeSearchInteractorTests: XCTestCase {
    
    // MARK: Unit Under Test

    private var recipeSearchInteractor: RecipeSearchInteractor!

    // MARK: Mocks
    
    private var recipeSearchService: StubRecipeSearchService!

    // MARK: Setup

    override func setUp() {
        let service = StubRecipeSearchService()
        let interactor = DefaultRecipeSearchInteractor(service: service)
        self.recipeSearchInteractor = interactor
        self.recipeSearchService = service
    }
    
    // MARK: Test Cases

    func testEmptySearch() {
        // Arrange
        let interactor = self.recipeSearchInteractor!
        // Act
        let recipes = try! interactor.perform(query: "")
        // Assert
        XCTAssertEqual(recipes.count, 3)
    }
    
    func testSearchQuery1() {
        // Arrange
        let interactor = self.recipeSearchInteractor!
        // Act
        let recipes = try! interactor.perform(query: "1")
        // Assert
        XCTAssertEqual(recipes.count, 1)
        XCTAssertTrue(recipes.first?.title?.contains("1") ?? false)
    }

    func testSearchQuery2() {
        // Arrange
        let interactor = self.recipeSearchInteractor!
        // Act
        let recipes = try! interactor.perform(query: "2")
        // Assert
        XCTAssertEqual(recipes.count, 1)
        XCTAssertTrue(recipes.first?.title?.contains("2") ?? false)
    }

    func testSearchQueryNoResults() {
        // Arrange
        let interactor = self.recipeSearchInteractor!
        // Act
        let recipes = try! interactor.perform(query: "4")
        // Assert
        XCTAssertEqual(recipes.count, 0)
    }
    
    func testSearchQueryError() {
        // Arrange
        let interactor = self.recipeSearchInteractor!
        self.recipeSearchService.shouldFail = true
        var expectedExceptionWasThrown = false
        // Act
        do {
            _ = try interactor.perform(query: "whatever")
        }
        catch is RecipeSearchError {
            expectedExceptionWasThrown = true
        }
        catch { }
        // Assert
        XCTAssertTrue(expectedExceptionWasThrown)
    }
    
}


// MARK: Fileprivate Types

fileprivate class StubRecipeSearchService: RecipeSearchService {
    
    var shouldFail = false
    
    func search(query: String) throws -> [Recipe] {
        guard !self.shouldFail else {
            throw RecipeSearchError()
        }
        if query.isEmpty {
            return self.recipes
        }
        else if query.contains("1") {
            return [self.recipe1]
        }
        else if query.contains("2") {
            return [self.recipe2]
        }
        else {
            return []
        }
    }
    
    private let recipe1 = Recipe(title: "Recipe 1")
    private let recipe2 = Recipe(title: "Recipe 2")
    private let recipe3 = Recipe(title: "Recipe 3")

    private lazy var recipes: [Recipe] = [ self.recipe1, self.recipe2, self.recipe3 ]

}
