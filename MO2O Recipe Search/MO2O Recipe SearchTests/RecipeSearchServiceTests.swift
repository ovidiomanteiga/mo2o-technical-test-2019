//
//  RecipeSearchServiceTests.swift
//  MO2O Recipe SearchTests
//
//  Created by Ovidio Manteiga Moar on 30/03/2019.
//  Copyright © 2019 ovidiomm. All rights reserved.
//

import XCTest

@testable import MO2O_Recipe_Search


class RecipeSearchServiceTests: XCTestCase {
    
    // MARK: Unit Under Test

    private var recipeSearchService: RecipeSearchService!

    // MARK: Mocks
    
    private var recipeSearchClient: StubRecipeSearchClient!

    // MARK: Setup

    override func setUp() {
        let client = StubRecipeSearchClient()
        let service = DefaultRecipeSearchService(client: client)
        self.recipeSearchService = service
        self.recipeSearchClient = client
    }
    
    // MARK: Test Cases

    func testEmptySearch() {
        // Arrange
        let service = self.recipeSearchService!
        // Act
        let recipes = try! service.search(query: "")
        // Assert
        XCTAssertEqual(recipes.count, 3)
    }
    
    func testSearchQuery1() {
        // Arrange
        let service = self.recipeSearchService!
        // Act
        let recipes = try! service.search(query: "1")
        // Assert
        XCTAssertEqual(recipes.count, 1)
        XCTAssertTrue(recipes.first?.title?.contains("1") ?? false)
    }

    func testSearchQuery2() {
        // Arrange
        let service = self.recipeSearchService!
        // Act
        let recipes = try! service.search(query: "2")
        // Assert
        XCTAssertEqual(recipes.count, 1)
        XCTAssertTrue(recipes.first?.title?.contains("2") ?? false)
    }
    
    func testSearchQueryNoResults() {
        // Arrange
        let service = self.recipeSearchService!
        // Act
        let recipes = try! service.search(query: "whatever")
        // Assert
        XCTAssertEqual(recipes.count, 0)
    }
    
    func testSearchQueryError() {
        // Arrange
        let service = self.recipeSearchService!
        self.recipeSearchClient.shouldFail = true
        var expectedExceptionWasThrown = false
        // Act
        do {
            _ = try service.search(query: "whatever")
        }
        catch {
            expectedExceptionWasThrown = true
        }
        // Assert
        XCTAssertTrue(expectedExceptionWasThrown)
    }

}


// MARK: Fileprivate Types

fileprivate class StubRecipeSearchClient: RecipeSearchClient {

    var shouldFail = false
    
    func perform(_ request: RecipeSearchRequestDTO) throws -> RecipeSearchResponseDTO? {
        guard !self.shouldFail else {
            throw RecipeSearchError()
        }
        guard let query = request.query else {
            return nil
        }
        if query.isEmpty {
            return self.recipes
        }
        else if query.contains("1") {
            return RecipeSearchResponseDTO(results: [self.recipe1])
        }
        else if query.contains("2") {
            return RecipeSearchResponseDTO(results: [self.recipe2])
        }
        else {
            return RecipeSearchResponseDTO(results: [])
        }
    }
    
    private let recipe1 = RecipeDTO(href: nil, ingredients: nil, thumbnail: nil, title: "Recipe 1")
    private let recipe2 = RecipeDTO(href: nil, ingredients: nil, thumbnail: nil, title: "Recipe 2")
    private let recipe3 = RecipeDTO(href: nil, ingredients: nil, thumbnail: nil, title: "Recipe 3")

    private lazy var recipes = RecipeSearchResponseDTO(results: [ self.recipe1, self.recipe2, self.recipe3 ])

}
