//
//  RecipeSearchPresenterTests.swift
//  MO2O Recipe SearchTests
//
//  Created by Ovidio Manteiga Moar on 29/03/2019.
//  Copyright © 2019 ovidiomm. All rights reserved.
//

import XCTest

@testable import MO2O_Recipe_Search


class RecipeSearchPresenterTests: XCTestCase {
    
    // MARK: Unit Under Test
    
    private var recipeSearchPresenter: DefaultRecipeSearchPresenter!

    // MARK: Mocks
    
    private var backgroundQueue: StubBackgroundQueue!
    private var recipeSearchPresenterDelegate: StubRecipeSearchPresenterDelegate!
    private var recipeSearchInteractor: StubRecipeSearchInteractor!

    // MARK: Setup

    override func setUp() {
        let interactor = StubRecipeSearchInteractor()
        let queue = StubBackgroundQueue()
        let presenter = DefaultRecipeSearchPresenter(background: queue, interactor: interactor)
        let delegate = StubRecipeSearchPresenterDelegate()
        presenter.delegate = delegate
        self.recipeSearchPresenter = presenter
        self.recipeSearchPresenterDelegate = delegate
        self.recipeSearchInteractor = interactor
        self.backgroundQueue = queue
    }
    
    // MARK: Test Cases
    
    func testSearchEmptyQuery() {
        // Arrange
        let presenter = self.recipeSearchPresenter!
        var recipes: RecipeSearchViewModel?
        let successExpectation = self.expectation(description: "Wait for success")
        self.recipeSearchPresenterDelegate.onSuccess = {
            recipes = $0
            successExpectation.fulfill()
        }
        // Act
        presenter.searchRecipes(query: "")
        // Assert
        self.waitForExpectations(timeout: 5)
        XCTAssertNotNil(recipes)
        XCTAssertEqual(recipes!.recipes.count, 3)
    }
    
    func testSearchQuery1() {
        // Arrange
        let presenter = self.recipeSearchPresenter!
        var recipes: RecipeSearchViewModel?
        let successExpectation = self.expectation(description: "Wait for success")
        self.recipeSearchPresenterDelegate.onSuccess = {
            recipes = $0
            successExpectation.fulfill()
        }
        // Act
        presenter.searchRecipes(query: "1")
        // Assert
        self.waitForExpectations(timeout: 5)
        XCTAssertNotNil(recipes)
        XCTAssertEqual(recipes!.recipes.count, 1)
    }

    func testSearchError() {
        // Arrange
        let presenter = self.recipeSearchPresenter!
        self.recipeSearchInteractor.shouldFail = true
        var error: RecipeSearchErrorViewModel?
        let errorExpectation = self.expectation(description: "Wait for error")
        self.recipeSearchPresenterDelegate.onError = {
            error = $0
            errorExpectation.fulfill()
        }
        // Act
        presenter.searchRecipes(query: "whatever")
        // Assert
        self.waitForExpectations(timeout: 5)
        XCTAssertNotNil(error)
    }

    func testSearchSameThread() {
        // Arrange
        let presenter = self.recipeSearchPresenter!
        let callingThread = Thread.current
        var callbackThread: Thread!
        let expectation = self.expectation(description: "Wait for success")
        self.recipeSearchPresenterDelegate.onSuccess = { _ in
            callbackThread = Thread.current
            expectation.fulfill()
        }
        // Act
        presenter.searchRecipes(query: "whatever")
        // Assert
        self.waitForExpectations(timeout: 1)
        XCTAssertEqual(callingThread, callbackThread)
    }
    
    func testSearchOnBackground() {
        // Arrange
        let presenter = self.recipeSearchPresenter!
        var searchOnBackground = false
        self.backgroundQueue.onOperationAdded = {
            searchOnBackground = true
        }
        // Act
        presenter.searchRecipes(query: "whatever")
        // Assert
        XCTAssertTrue(searchOnBackground)
    }

}


// MARK: Fileprivate Types

fileprivate class StubRecipeSearchInteractor: RecipeSearchInteractor {
    
    var shouldFail = false
    
    func perform(query: String) throws -> [Recipe] {
        guard !self.shouldFail else {
            throw RecipeSearchError()
        }
        if query.contains("1") {
            return [recipe1]
        }
        return self.recipes
    }
    
    private let recipe1 = Recipe(title: "Recipe 1")
    private let recipe2 = Recipe(title: "Recipe 2")
    private let recipe3 = Recipe(title: "Recipe 3")

    private lazy var recipes: [Recipe] = [ self.recipe1, self.recipe2, self.recipe3 ]

}


fileprivate class StubRecipeSearchPresenterDelegate: RecipeSearchPresenterDelegate {
    
    var onError: ((RecipeSearchErrorViewModel) -> Void)?
    var onSuccess: ((RecipeSearchViewModel) -> Void)?

    func presenterDidLoadRecipes(recipes: RecipeSearchViewModel) {
        self.onSuccess?(recipes)
    }
    
    func presenterDidFailLoadingRecipes(error: RecipeSearchErrorViewModel) {
        self.onError?(error)
    }
    
}


fileprivate class StubBackgroundQueue: OperationQueue {
    
    var onOperationAdded: (() -> Void)?
    
    override func addOperation(_ block: @escaping () -> Void) {
        super.addOperation(block)
        self.onOperationAdded?()
    }
    
}
