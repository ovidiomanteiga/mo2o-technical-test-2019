//
//  AppDelegate.swift
//  MO2O Recipe Search
//
//  Created by Ovidio Manteiga Moar on 29/03/2019.
//  Copyright © 2019 ovidiomm. All rights reserved.
//

import UIKit


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    // MARK: - Internal Properties
    
    var window: UIWindow?

    // MARK: - UIApplicationDelegate Override
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        let window = UIWindow(frame: UIScreen.main.bounds)
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let initialViewController = storyboard.instantiateInitialViewController()
        if let mainNavigationController = initialViewController as? MainNavigationController {
            mainNavigationController.presenter = self.composer.recipesSearchPresenter
        }
        window.rootViewController = initialViewController
        window.makeKeyAndVisible()
        self.window = window
        return true
    }
    
    // MARK: - Private Properties

    private lazy var composer = Composer()

}
