//
//  Composition.swift
//  MO2O Recipe Search
//
//  Created by Ovidio Manteiga Moar on 30/03/2019.
//  Copyright © 2019 ovidiomm. All rights reserved.
//

import Foundation


class Composer {
    
    let backgroundQueue = OperationQueue()

    var recipesSearchClient: RecipeSearchClient {
        return RecipePuppySearchClient()
    }

    var recipesSearchInteractor: RecipeSearchInteractor {
        return DefaultRecipeSearchInteractor(service: self.recipesSearchService)
    }
    
    var recipesSearchPresenter: RecipeSearchPresenter {
        return DefaultRecipeSearchPresenter(background: self.backgroundQueue,
                                            interactor: self.recipesSearchInteractor)
    }

    var recipesSearchService: RecipeSearchService {
        //return FakeRecipeSearchService()
        return DefaultRecipeSearchService(client: self.recipesSearchClient)
    }

}
