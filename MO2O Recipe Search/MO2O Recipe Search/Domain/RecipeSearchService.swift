//
//  RecipeSearchService.swift
//  MO2O Recipe Search
//
//  Created by Ovidio Manteiga Moar on 29/03/2019.
//  Copyright © 2019 ovidiomm. All rights reserved.
//

import Foundation


protocol RecipeSearchService {
    
    func search(query: String) throws -> [Recipe]

}
