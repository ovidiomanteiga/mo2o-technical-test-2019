//
//  RecipeSearchInteractor.swift
//  MO2O Recipe Search
//
//  Created by Ovidio Manteiga Moar on 29/03/2019.
//  Copyright © 2019 ovidiomm. All rights reserved.
//

import Foundation


protocol RecipeSearchInteractor {
    
    func perform(query: String) throws -> [Recipe]
    
}


class DefaultRecipeSearchInteractor: RecipeSearchInteractor {
    
    init(service: RecipeSearchService) {
        self.service = service
    }
    
    func perform(query: String) throws -> [Recipe] {
        return try self.service.search(query: query)
    }
 
    private let service: RecipeSearchService
    
}


struct RecipeSearchError: Error { }
