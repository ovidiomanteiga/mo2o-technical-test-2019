//
//  Recipe.swift
//  MO2O Recipe Search
//
//  Created by Ovidio Manteiga Moar on 29/03/2019.
//  Copyright © 2019 ovidiomm. All rights reserved.
//

import Foundation


struct Recipe {
    
    // MARK: Internal Properties

    let iconURL: URL?
    let ingredients: [Ingredient]?
    let link: URL?
    let title: String?
    
    // MARK: Initialization

    init(iconURL: URL? = nil, ingredients: [Ingredient]? = nil, link: URL? = nil, title: String? = nil) {
        self.iconURL = iconURL
        self.ingredients = ingredients
        self.link = link
        self.title = title
    }

}


typealias Ingredient = String
