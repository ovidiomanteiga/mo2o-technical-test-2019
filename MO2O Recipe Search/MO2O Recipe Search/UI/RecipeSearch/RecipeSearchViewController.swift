//
//  RecipeSearchViewController.swift
//  MO2O Recipe Search
//
//  Created by Ovidio Manteiga Moar on 29/03/2019.
//  Copyright © 2019 ovidiomm. All rights reserved.
//

import UIKit


class RecipeSearchViewController: UIViewController {

    // MARK: - Internal Properties
    
    var presenter: RecipeSearchPresenter? {
        didSet {
            self.presenter?.delegate = self
        }
    }

    // MARK: - UIViewController Override
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.tableFooterView = UIView(frame: .zero)
        self.searchBar.delegate = self
        self.searchBar.becomeFirstResponder()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let controller = segue.destination as? RecipeDetailViewController {
            controller.recipe = self.lastSelectedRecipe
        }
    }

    // MARK: - IBOutlets
    
    @IBOutlet
    private weak var loadingView: UIView!
    @IBOutlet
    private weak var messageLabel: UILabel!
    @IBOutlet
    private weak var messageView: UIView!
    @IBOutlet
    private weak var searchBar: UISearchBar!
    @IBOutlet
    private weak var tableView: UITableView!
    
    // MARK: - Private Properties

    fileprivate var lastQuery = ""
    fileprivate var lastSelectedRecipe: RecipeViewModel?
    fileprivate var loading = false {
        didSet {
            self.loadingView.isHidden = !self.loading
        }
    }
    fileprivate var recipes: RecipeSearchViewModel? {
        didSet {
            self.tableView.reloadData()
            if let recipes = self.recipes, recipes.recipes.count == 0 {
                let query = self.lastQuery
                self.messageLabel.text = "🤷‍♂️\nNo recipes match the query \n\"\(query)\""
                self.showMessage = true
            }
        }
    }
    fileprivate var showMessage = false {
        didSet {
            self.messageView.isHidden = !self.showMessage
        }
    }

    // MARK: - Private Methods
    
    fileprivate func showError(_ error: RecipeSearchErrorViewModel) {
        self.messageLabel.text = "🛑 An error occurred!"
        self.showMessage = true
    }
    
}


// MARK: - RecipeSearchPresenterDelegate Implementation

extension RecipeSearchViewController: RecipeSearchPresenterDelegate {

    func presenterDidLoadRecipes(recipes: RecipeSearchViewModel) {
        self.loading = false
        self.recipes = recipes
    }
    
    func presenterDidFailLoadingRecipes(error: RecipeSearchErrorViewModel) {
        self.loading = false
        self.showError(error)
    }
    
}


// MARK: - UISearchBarDelegate Implementation

extension RecipeSearchViewController: UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.showMessage = false
        self.loading = true
        self.lastQuery = searchText
        self.presenter?.searchRecipes(query: searchText)
    }
    
}


// MARK: - UITableViewDataSource Implementation

extension RecipeSearchViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.recipes?.recipes.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "default", for: indexPath)
        self.map(to: cell, at: indexPath)
        return cell
    }

    private func map(to cell: UITableViewCell, at indexPath: IndexPath) {
        let recipeViewModel = self.recipes?.recipes[indexPath.row]
        cell.textLabel?.text = recipeViewModel?.title
    }

}


// MARK: - UITableViewDelegate Implementation

extension RecipeSearchViewController: UITableViewDelegate {

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let recipeViewModel = self.recipes?.recipes[indexPath.row]
        self.lastSelectedRecipe = recipeViewModel
        self.performSegue(withIdentifier: "showDetail", sender: nil)
    }
    
}
