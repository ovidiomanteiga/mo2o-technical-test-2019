//
//  RecipesViewModels.swift
//  MO2O Recipe Search
//
//  Created by Ovidio Manteiga Moar on 30/03/2019.
//  Copyright © 2019 ovidiomm. All rights reserved.
//

import Foundation


struct RecipeViewModel {
    
    let iconURL: URL?
    let ingredientsText: String?
    let link: URL?
    let title: String?

}


struct RecipeSearchViewModel {

    let recipes: [RecipeViewModel]

}


struct NoRecipesViewModel {

    let message: String

}


struct RecipeSearchErrorViewModel {

    let message: String

}
