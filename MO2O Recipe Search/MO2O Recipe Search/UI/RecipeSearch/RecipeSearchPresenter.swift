//
//  RecipeSearchPresenter.swift
//  MO2O Recipe Search
//
//  Created by Ovidio Manteiga Moar on 30/03/2019.
//  Copyright © 2019 ovidiomm. All rights reserved.
//

import Foundation


protocol RecipeSearchPresenter {
    
    init(background: OperationQueue, interactor: RecipeSearchInteractor)
    
    var delegate: RecipeSearchPresenterDelegate? { get set }
    
    func searchRecipes(query: String)

}


protocol RecipeSearchPresenterDelegate {
    
    func presenterDidLoadRecipes(recipes: RecipeSearchViewModel)
    func presenterDidFailLoadingRecipes(error: RecipeSearchErrorViewModel)
    
}


class DefaultRecipeSearchPresenter: RecipeSearchPresenter {
    
    // MARK: - Initialization
    
    required init(background: OperationQueue, interactor: RecipeSearchInteractor) {
        self.background = background
        self.interactor = interactor
    }

    // MARK: - Internal Properties

    var delegate: RecipeSearchPresenterDelegate?
    
    // MARK: - Internal Methods

    func searchRecipes(query: String) {
        self.callingQueue = OperationQueue.current
        self.background.addOperation {
            self.doSearchRecipes(query: query)
        }
    }
    
    // MARK: - Private Properties
    
    private let background: OperationQueue
    private let interactor: RecipeSearchInteractor
    
    private var callingQueue: OperationQueue?

    // MARK: - Private Methods

    private func doSearchRecipes(query: String) {
        do {
            let recipes = try self.interactor.perform(query: query)
            self.onSuccess(recipes: recipes)
        }
        catch let error {
            self.onError(error: error)
        }
    }
    
    private func onError(error: Error) {
        let errorViewModel = self.mapToViewModel(error: error)
        self.callingQueue?.addOperation {
            self.delegate?.presenterDidFailLoadingRecipes(error: errorViewModel)
        }
    }

    private func onSuccess(recipes: [Recipe]) {
        let recipesViewModel = self.mapToViewModel(recipes: recipes)
        self.callingQueue?.addOperation {
            self.delegate?.presenterDidLoadRecipes(recipes: recipesViewModel)
        }
    }
    
    private func mapToViewModel(error: Error) -> RecipeSearchErrorViewModel {
        return RecipeSearchErrorViewModel(message: error.localizedDescription)
    }
    
    private func mapToViewModel(recipes: [Recipe]) -> RecipeSearchViewModel {
        let recipeViewModels = recipes.map(self.mapToViewModel)
        return RecipeSearchViewModel(recipes: recipeViewModels)
    }

    private func mapToViewModel(recipe: Recipe) -> RecipeViewModel {
        let ingredientsText = recipe.ingredients?.joined(separator: ", ")
        return RecipeViewModel(iconURL: recipe.iconURL,
                               ingredientsText: ingredientsText,
                               link: recipe.link,
                               title: recipe.title)
    }

}
