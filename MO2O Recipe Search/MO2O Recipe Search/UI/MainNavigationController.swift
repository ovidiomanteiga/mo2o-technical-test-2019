//
//  MainNavigationController.swift
//  MO2O Recipe Search
//
//  Created by Ovidio Manteiga Moar on 30/03/2019.
//  Copyright © 2019 ovidiomm. All rights reserved.
//

import UIKit


class MainNavigationController: UINavigationController {
    
    // MARK: - Internal Properties
    
    var presenter: RecipeSearchPresenter?

    // MARK: - UIViewController Override
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let controller = self.viewControllers.first as? RecipeSearchViewController {
            controller.presenter = self.presenter
        }
    }
    
}
