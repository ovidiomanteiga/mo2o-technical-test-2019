//
//  RecipeDetailViewController.swift
//  MO2O Recipe Search
//
//  Created by Ovidio Manteiga Moar on 07/04/2019.
//  Copyright © 2019 ovidiomm. All rights reserved.
//

import UIKit


class RecipeDetailViewController: UIViewController {

    // MARK: - Internal Properties
    
    var recipe: RecipeViewModel?

    // MARK: - UIViewController Override

    override func viewDidLoad() {
        super.viewDidLoad()
        self.mapViewModel()
    }

    // MARK: - IBOutlets
    
    @IBOutlet
    fileprivate weak var imageView: UIImageView!
    @IBOutlet
    fileprivate weak var ingredientsLabel: UILabel!
    @IBOutlet
    fileprivate weak var linkButton: UIButton!
    @IBOutlet
    fileprivate weak var titleLabel: UILabel!

    // MARK: - IBAction
    
    @IBAction
    fileprivate func didTapOnOpenInBrowserButton() {
        guard let link = self.recipe?.link
            else { return }
        UIApplication.shared.open(link, options: [:])
    }

    // MARK: - Private Methods

    private func mapViewModel() {
        self.ingredientsLabel.text = self.recipe?.ingredientsText
        self.titleLabel.text = self.recipe?.title
        self.mapImage()
    }

    private func mapImage() {
        self.imageView.isHidden = true
        guard let iconURL = self.recipe?.iconURL
            else { return }
        OperationQueue().addOperation {
            let data = try! Data(contentsOf: iconURL)
            let image = UIImage(data: data)
            OperationQueue.main.addOperation {
                self.imageView.image = image
                self.imageView.isHidden = image == nil
            }
        }
    }

}
