//
//  RecipeSearchMapper.swift
//  MO2O Recipe Search
//
//  Created by Ovidio Manteiga Moar on 30/03/2019.
//  Copyright © 2019 ovidiomm. All rights reserved.
//

import Foundation


class RecipeSearchMapper {
    
    func map(_ dto: RecipeSearchResponseDTO?) -> [Recipe] {
        guard let dto = dto else { return [] }
        return dto.results.map(self.map)
    }

    func map(_ dto: RecipeDTO) -> Recipe {
        let iconURL = URL(string: dto.thumbnail ?? "")
        let ingredients = dto.ingredients?.split(separator: ",").map { String($0) }
        let link = URL(string: dto.href ?? "")
        let title = dto.title
        return Recipe(iconURL: iconURL, ingredients: ingredients, link: link, title: title)
    }

}
