//
//  FakeRecipeSearchService.swift
//  MO2O Recipe Search
//
//  Created by Ovidio Manteiga Moar on 30/03/2019.
//  Copyright © 2019 ovidiomm. All rights reserved.
//

import Foundation


class FakeRecipeSearchService: RecipeSearchService {
    
    // MARK: - Internal Methods
    
    func search(query: String) throws -> [Recipe] {
        Thread.sleep(forTimeInterval: 1.0)
        let lowerQuery = query.lowercased()
        return self.recipes.filter {
            query.isEmpty || ($0.title?.lowercased().contains(lowerQuery) ?? false)
        }
    }
    
    // MARK: - Private Properties
    
    private let recipes: [Recipe] = [
        Recipe(title: "Batatas ao murro"),
        Recipe(title: "Clams marinière"),
        Recipe(title: "Chili con carne"),
        Recipe(title: "California rolls"),
        Recipe(title: "Filloas con rexós"),
        Recipe(title: "Fondue chinoise"),
        Recipe(title: "Paella de marisco"),
        Recipe(title: "Steak tartare"),
    ]

}
