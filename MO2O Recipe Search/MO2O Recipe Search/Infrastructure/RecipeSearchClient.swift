//
//  RecipeSearchClient.swift
//  MO2O Recipe Search
//
//  Created by Ovidio Manteiga Moar on 30/03/2019.
//  Copyright © 2019 ovidiomm. All rights reserved.
//

import Foundation


protocol RecipeSearchClient {
    
    func perform(_: RecipeSearchRequestDTO) throws -> RecipeSearchResponseDTO?
    
}


struct RecipeSearchRequestDTO: Encodable {
    
    let query: String?

}


struct RecipeSearchResponseDTO: Decodable {

    let results: [RecipeDTO]

}


struct RecipeDTO: Decodable {

    let href: String?
    let ingredients: String?
    let thumbnail: String?
    let title: String?

}


extension String: Error { }
