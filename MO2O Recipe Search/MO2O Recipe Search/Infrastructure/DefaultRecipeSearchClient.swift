//
//  DefaultRecipeSearchClient.swift
//  MO2O Recipe Search
//
//  Created by Ovidio Manteiga Moar on 02/03/2019.
//  Copyright © 2019 ovidiomm. All rights reserved.
//

import Foundation


class RecipePuppySearchClient: RecipeSearchClient {
    
    // MARK: - RecipeSearchClient Implementation
    
    func perform(_ request: RecipeSearchRequestDTO) throws -> RecipeSearchResponseDTO? {
        let session = URLSession.shared
        let url = try self.map(request)
        let task = session.dataTask(with: url, completionHandler: self.onCompletion)
        self.group.enter()
        task.resume()
        self.group.wait()
        if let error = self.error {
            throw error
        }
        return self.response
    }
    
    // MARK: - Private Properties

    private let group = DispatchGroup()

    private lazy var baseURL: URLComponents = {
        var urlComponents = URLComponents()
        urlComponents.scheme = "http"
        urlComponents.host = "www.recipepuppy.com"
        urlComponents.path = "/api"
        return urlComponents
    }()

    private var error: Error?
    private var response: RecipeSearchResponseDTO?

    // MARK: - Private Methods
    
    private func map(_ request: RecipeSearchRequestDTO) throws -> URL {
        let query = request.query ?? ""
        var baseURL = self.baseURL
        baseURL.queryItems = [ URLQueryItem(name: "q", value: query) ]
        guard let url = baseURL.url else {
            throw "🛑 Malformed API URL!"
        }
        return url
    }
    
    private func onCompletion(data: Data?, response: URLResponse?, error: Error?) {
        guard let data = data else {
            self.error = error
            return
        }
        do {
            self.response = try JSONDecoder().decode(RecipeSearchResponseDTO.self, from: data)
        } catch let error {
            self.error = error
        }
        self.group.leave()
    }

}
