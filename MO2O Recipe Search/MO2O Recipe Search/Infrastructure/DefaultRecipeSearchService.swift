//
//  DefaultRecipeSearchService.swift
//  MO2O Recipe Search
//
//  Created by Ovidio Manteiga Moar on 30/03/2019.
//  Copyright © 2019 ovidiomm. All rights reserved.
//

import Foundation


class DefaultRecipeSearchService: RecipeSearchService {
    
    // MARK: - Initialization
    
    init(client: RecipeSearchClient) {
        self.client = client
    }

    // MARK: - Internal Methods

    func search(query: String) throws -> [Recipe] {
        let request = RecipeSearchRequestDTO(query: query)
        let response = try self.client.perform(request)
        return self.mapper.map(response)
    }
    
    // MARK: - Private Properties
    
    private let client: RecipeSearchClient
    private let mapper: RecipeSearchMapper = RecipeSearchMapper()

}
